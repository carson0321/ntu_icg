﻿#pragma strict
public var isQuit=false;
public var displayText=false;
public var tank : GameObject; 

private var tankOrigin : Quaternion;
private var stringToEdit : String = "武器 **\n火力 ***\n速度 **\n裝甲 *";

function Start () {
	tankOrigin=tank.transform.rotation;
}

function OnGUI () {
	if(displayText) stringToEdit = GUI.TextArea (Rect (220, 100, 100, 75), stringToEdit,30);
}

function OnMouseEnter(){
	//change text color
	renderer.material.color=Color.red;
	displayText=true;
}

function OnMouseExit(){
	//change text color
	renderer.material.color=Color.white;
	displayText=false;
}

function OnMouseUp(){
	Application.LoadLevel("M1");
}

function Update(){
	//quit game if escape key is pressed
	if (Input.GetKey(KeyCode.Escape)) {
    		Application.Quit();
	}
	if(displayText) tank.transform.Rotate(0,2,0);
	else tank.transform.rotation=tankOrigin;
}