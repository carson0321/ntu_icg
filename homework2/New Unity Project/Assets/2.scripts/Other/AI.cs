﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Reflection;

public class AI : MonoBehaviour {
	public Transform target;
	public int moveSpeed;
	public int rotationSpeed;
	public int maxDistance;

	private Transform myTransform;


	//Health varibles
	private int currentHealth;
	public int maxHealth;
	public RectTransform healthTransform;
	public Text healthText;
	public Image visualHealth;
	private float cachedY;
	private float minXValue;
	private float maxXValue;
	private float currentXValue;
	public float cooldown;
	private bool onCD;


	public GameObject explosion;

	void Awake(){
		myTransform = transform;
	}

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.FindGameObjectWithTag("Player");
		target = go.transform;
		maxDistance = 13;
		moveSpeed = 6;
		rotationSpeed = 5;

		onCD = false;
		cachedY = healthTransform.position.y; 
		maxXValue = healthTransform.position.x;
		minXValue = healthTransform.position.x - healthTransform.rect.width;
		maxHealth = 120;
		currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {

		if(!myTransform.Find ("Fire1").active){
			Debug.DrawLine (target.position, myTransform.position,Color.yellow);
			myTransform.rotation = Quaternion.Slerp(myTransform.rotation,Quaternion.LookRotation (target.position-myTransform.position), rotationSpeed * Time.deltaTime);

			if (Vector3.Distance (target.position, myTransform.position) > maxDistance) {
						myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
			}
		}
		if (currentHealth <= 0) {
			myTransform.Find ("Fire1").active = true;
		}
	}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "ball1")
		{
			if (!onCD && currentHealth > 0)
			{
				StartCoroutine(CoolDownDmg());
				Health -= 15;
			}
		}
		if (other.gameObject.tag == "ball2")
		{
			if (!onCD && currentHealth > 0)
			{
				StartCoroutine(CoolDownDmg());
				Health -= 20;
			}
		}
		if (other.gameObject.tag == "ball3")
		{
			if (!onCD && currentHealth > 0)
			{
				StartCoroutine(CoolDownDmg());
				Health -= 30;
			}
		}
		if (Health <= 0) Health = 0;
		Instantiate(explosion, other.contacts[0].point, Quaternion.identity);
		Destroy(other.gameObject);
	}


	public int Health
	{
		get { return currentHealth; }
		set
		{
			currentHealth = value;
			HandleHealthbar();
		}
	}
	private void HandleHealthbar()
	{   
		healthText.text = "Enemy: " + currentHealth;
		currentXValue = Map(currentHealth, 0, maxHealth, minXValue, maxXValue);
		healthTransform.position = new Vector3(currentXValue, cachedY);
		
		/*if (currentHealth > maxHealth / 2)
		{
			visualHealth.color = new Color32((byte)Map(currentHealth, maxHealth / 2,maxHealth, 255, 0), 255, 0, 255);
		}
		else
		{
			visualHealth.color = new Color32(255, (byte)Map(currentHealth, 0, maxHealth / 2, 0, 255), 0, 255);
		}*/
	}
	IEnumerator CoolDownDmg()
	{
		onCD = true; 
		yield return new WaitForSeconds(cooldown);
		onCD = false;
	}
	
	public float Map(float x, float in_min, float in_max, float out_min, float out_max)
	{
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
}
