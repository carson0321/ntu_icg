﻿#pragma strict
var fpsMode : boolean = false;
var control : boolean = true;

function Update () {
	if (Input.GetKeyDown("space")) {
		fpsMode = !fpsMode;
     	GameObject.Find("Camera").camera.enabled = fpsMode;
     	GameObject.Find("FPS").camera.enabled = !fpsMode;
    }
    if (Input.GetKey(KeyCode.Q)){
    	if(GameObject.Find("Camera").camera.enabled){
    		GameObject.Find("Camera").camera.enabled = false;
    		control=true;
    	}
    	if(GameObject.Find("FPS").camera.enabled){
    		GameObject.Find("FPS").camera.enabled = false;
    		control=false;
    	}
     	GameObject.Find("Black").camera.enabled = true;
    }
    if (Input.GetKeyUp(KeyCode.Q)){
     	GameObject.Find("Black").camera.enabled = false;
     	if(control) GameObject.Find("Camera").camera.enabled = true;
     	else GameObject.Find("FPS").camera.enabled = true;
    }
}

function Start () {
     GameObject.Find("Camera").camera.enabled = true;
     GameObject.Find("FPS").camera.enabled = false;
     GameObject.Find("Black").camera.enabled = false;
     GameObject.Find("deadSceen").camera.enabled = false;
     GameObject.Find ("win").camera.enabled = false;
}