public var explosion : GameObject;

private var nextFire = 0.0;
static var test : boolean = false;

function Start(){
	if(Time.time > nextFire){
		if(test==true){
			Destroy(gameObject);
			test=false;
		}
		else{
			Destroy(gameObject,5);
		}
	}
	else{
		Destroy(gameObject);
	}
}

function OnCollisionEnter(collision : Collision){
	Instantiate(explosion, collision.contacts[0].point, Quaternion.identity);
	//Instantiate (explosion, transform.position, transform.rotation);
	Destroy(gameObject);
}