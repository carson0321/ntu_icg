﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Reflection;

public class TankControl : MonoBehaviour {

	public float speed  = 10.0f;
	public float rotationSpeed  = 30.0f;
	
	private int currentHealth;
	public int maxHealth;
	public RectTransform healthTransform;
	public Text healthText;
	public Image visualHealth;
	private float cachedY;
	private float minXValue;
	private float maxXValue;
	private float currentXValue;
	public float cooldown;
	private bool onCD;
	

	public Image damageImage;
	private bool damaged;
	public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
	public float flashSpeed = 5f;

	private bool displayTextHealth=false;
	private GUIStyle style;

	public int Health
	{
		get { return currentHealth; }
		set
		{
			currentHealth = value;
			HandleHealthbar();
		}
	}

	void OnGUI () {
		if (displayTextHealth) {
			GUI.Label (new Rect (380, 220, 70, 40), "生命恢復",style);
		}
	}
	public void stopDisplay() { 
		displayTextHealth=false;
	}
	private void initialStyle(){
		style = new GUIStyle();
		style.fontSize = 60;
		style.normal.textColor = Color.green;
		style.fontStyle = FontStyle.Bold;
	}

	// Use this for initialization
	void Start () {
		onCD = false;
		cachedY = healthTransform.position.y; 
		maxXValue = healthTransform.position.x;
		minXValue = healthTransform.position.x - healthTransform.rect.width;
		maxHealth = 100;
		currentHealth = maxHealth;
		initialStyle ();
	}

	// Update is called once per frame
	void Update () {
		float move = Input.GetAxis ("Vertical") * speed * Time.deltaTime;
		transform.Translate (0, 0, move);
		
		float rotation = Input.GetAxis ("Horizontal") * rotationSpeed * Time.deltaTime;
		transform.Rotate (0, rotation, 0);
		if (currentHealth <= 0) {
			GameObject.Find ("deadSceen").camera.enabled = true;
			GameObject.FindGameObjectWithTag("Player").active = false;
		}
		if(damaged) damageImage.color = flashColour;
		else damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
		damaged = false;
	}

	private void HandleHealthbar()
	{   
		healthText.text = "Health: " + currentHealth;
		currentXValue = Map(currentHealth, 0, maxHealth, minXValue, maxXValue);
		healthTransform.position = new Vector3(currentXValue, cachedY);
		
		if (currentHealth > maxHealth / 2)
		{
			visualHealth.color = new Color32((byte)Map(currentHealth, maxHealth / 2,maxHealth, 255, 0), 255, 0, 255);
		}
		else
		{
			visualHealth.color = new Color32(255, (byte)Map(currentHealth, 0, maxHealth / 2, 0, 255), 0, 255);
		}
	}

	public GameObject explosion;
	void OnCollisionEnter(Collision other){
		if (other.gameObject.tag == "Damage")
		{
			if (!onCD && currentHealth > 0)
			{
				damaged=true;
				StartCoroutine(CoolDownDmg());
				Health -= 20;
			}
		}
		if (other.gameObject.tag == "firefloor")
		{
			if (!onCD && currentHealth > 0)
			{
				damaged=true;
				StartCoroutine(CoolDownDmg());
				Health -= 5;
				other.gameObject.active=false;
			}
		}
		if (other.gameObject.tag == "b1")
		{
			if (!onCD && currentHealth < maxHealth)
			{
				StartCoroutine(CoolDownDmg());
				Health += 10;
				other.gameObject.active=false;
				displayTextHealth=true;
				Invoke("stopDisplay",2);
			}
		}
		if (other.gameObject.tag == "b2")
		{
			if (!onCD && currentHealth < maxHealth)
			{
				StartCoroutine(CoolDownDmg());
				Health += 20;
				other.gameObject.active=false;
				displayTextHealth=true;
				Invoke("stopDisplay",2);
			}
		}
		if (Health <= 0) Health = 0;
		Instantiate(explosion, other.contacts[0].point, Quaternion.identity);
		Destroy(other.gameObject);
	}
	IEnumerator CoolDownDmg()
	{
		onCD = true; 
		yield return new WaitForSeconds(cooldown);
		onCD = false;
	}

	public float Map(float x, float in_min, float in_max, float out_min, float out_max)
	{
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
}
