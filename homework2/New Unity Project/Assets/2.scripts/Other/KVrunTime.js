﻿var isPaused : boolean = false;
var startTime : float;
static var timeRemaining : float;
 
var minutes : int;
var seconds : int;
var timeStr : String;

var style : GUIStyle;
var windowRect = Rect(400,200,200,160);

var WindowSwitch : boolean = false;
var gameOver : boolean = false;
static var jump : boolean = false;

function Awake () {
    style.fontSize = 40;  //改大小
    style.fontStyle = FontStyle.Bold;  //改成粗體，詳細可以搜尋FontStyle
}


function OnGUI (){
   if(jump){ 
      windowRect = GUI.Window (0, windowRect, WindowContain,"選單");
      GameObject.FindGameObjectWithTag("Player").GetComponent("KVTankControl").enabled = false;
      GameObject.FindGameObjectWithTag("gun").GetComponent("MainGunFire").enabled = false;
   }
   if(!gameOver){
      style.normal.textColor = Color.blue; //改顏色
      GUI.Label (Rect (380, 10, 100, 20),"倒數時間還有 "+timeStr,style);
   }
   if(WindowSwitch){
      style.normal.textColor = Color.red;
      GUI.Label (Rect (400, 150, 100, 20),"時間到了!!",style);
      windowRect = GUI.Window (0, windowRect, WindowContain,"遊戲結束");
      GameObject.FindGameObjectWithTag("Player").active = false;
      GameObject.Find ("deadSceen").camera.enabled = true;
      GameObject.FindGameObjectWithTag("end").GetComponent("fireScript").enabled = false;
   }
   else{
      if(GameObject.Find ("deadSceen").camera.enabled){
          gameOver=true;
          style.normal.textColor = Color.red;
          GUI.Label (Rect (400, 150, 100, 20),"您已經 GG了!!",style);
          windowRect = GUI.Window (0, windowRect, WindowContain,"遊戲結束");
          GameObject.FindGameObjectWithTag("end").GetComponent("fireScript").enabled = false;
      }
  }
  if((GameObject.FindGameObjectWithTag("fire1").active)&&(GameObject.FindGameObjectWithTag("fire2").active)
     &&(GameObject.FindGameObjectWithTag("fire1").active)){
     gameOver=true;
     style.normal.textColor = Color.red;
     GUI.Label (Rect (420, 150, 100, 20),"您贏了!!",style);
     windowRect = GUI.Window (0, windowRect, WindowContain,"遊戲結束");
     GameObject.FindGameObjectWithTag("Player").active = false;
     GameObject.FindGameObjectWithTag("end").GetComponent("fireScript").enabled = false;
     GameObject.Find ("win").camera.enabled = true;
  }
}

function WindowContain(windowID : int) {
	if(gameOver){
    	if(GUI.Button (Rect (50,30,100,30), "關閉視窗")){
        	Application.Quit();
    	}
    	if(GUI.Button (Rect (50,70,100,30), "再玩一次")){
        	Application.LoadLevel("Kv1");
    	}
    	if(GUI.Button (Rect (50,110,100,30), "返回選單")){
        	Application.LoadLevel("choice");
    	}
	}
	else {
		if(GUI.Button (Rect (50,30,100,30), "關閉視窗")){
        	Application.Quit();
    	}
    	if(GUI.Button (Rect (50,70,100,30), "繼續遊戲")){
        	jump=false;
        	GameObject.FindGameObjectWithTag("Player").GetComponent("KVTankControl").enabled = true;
        	GameObject.FindGameObjectWithTag("gun").GetComponent("MainGunFire").enabled = true;
    	}
    	if(GUI.Button (Rect (50,110,100,30), "返回選單")){
    		jump=false;
        	Application.LoadLevel("choice");
    	}
	}
}

function Start(){
    startTime =80.0;
}

function Update() {
	if (!isPaused){
    	DoCountdown();
    }
    if(Input.GetKeyDown("escape")) jump=true;
}

function DoCountdown() {
    timeRemaining = startTime - Time.timeSinceLevelLoad;
    ShowTime();

    if (timeRemaining < 0){
    	timeRemaining = 0.0;
   		isPaused = true;
    	TimeIsUp();
    }
}

function PauseClock(){
    isPaused = true;
}

function UnpauseClock(){
    isPaused = false;
}

function TimeIsUp(){
       if(timeRemaining == 0){
       		WindowSwitch=true;
       		gameOver=true;
       }
}

function ShowTime(){
    minutes = timeRemaining/60;
    seconds = timeRemaining % 60;
    timeStr = minutes.ToString() + ":";
    timeStr += seconds.ToString("D2");
}