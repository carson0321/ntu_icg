
public var speed : float = 5.0f;
public var rotationSpeed : float = 20.0f;

function Update () 
{
	var move : float = Input.GetAxis("Vertical") * speed * Time.deltaTime;
	transform.Translate(0, 0, move);

	var rotation : float = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
	transform.Rotate(0, rotation, 0);
}
