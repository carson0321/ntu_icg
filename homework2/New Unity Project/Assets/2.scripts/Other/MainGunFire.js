public var bulletPrefab1 : GameObject;
public var bulletPrefab2 : GameObject;
public var bulletPrefab3 : GameObject;
public var bulletPrefab : GameObject;
public var style : GUIStyle;  

private var controlBullet = 1;
private var timer = 2.0f;
private var displayText=false;
private var stringToEdit : String = "切換火砲 !!為";
private var bulletKind : String;


function Awake () {
	bulletPrefab=bulletPrefab1;
	
    style.fontSize = 40;  //改大小
    style.normal.textColor = Color.white; //改顏色
    style.fontStyle = FontStyle.Bold;  //改成粗體，詳細可以搜尋FontStyle
}

function OnGUI () {
	//if(displayText) stringToEdit = GUI.TextArea (Rect (510, 200, 70, 20), stringToEdit, 25);
	if(displayText) GUI.Label (Rect (380, 280, 70, 40), stringToEdit+bulletKind , style);
}


function Update () {
	
		if (Input.GetButtonDown("Fire1")) {		
			particleEmitter.Emit();
			Fire();				
		}
		if(Input.GetAxis("Mouse ScrollWheel") > 0){
			displayText=true;
			chagngeBullet();
			if(controlBullet==3) controlBullet=1;
			else controlBullet++;
			Invoke("stopDisplay",2);
		}
		else if(Input.GetAxis("Mouse ScrollWheel") < 0){
			displayText=true;
			chagngeBullet();
			if(controlBullet==1) controlBullet=3;
			else controlBullet--;
			Invoke("stopDisplay",2);
		}
}

function Fire() { 
    var bulletClone : GameObject =  
        Instantiate(bulletPrefab, transform.position, transform.rotation); 
        
    bulletClone.rigidbody.velocity = transform.forward * 100; 
 
    Physics.IgnoreCollision(transform.root.collider, bulletClone.collider); 
}

function stopDisplay() { 
	displayText=false;
}

function chagngeBullet() { 
	if(controlBullet==1){
			bulletPrefab=bulletPrefab2;
			bulletKind="火砲二";
			style.normal.textColor = Color.white;
	}
	else if(controlBullet==2){
			bulletPrefab=bulletPrefab3;
			bulletKind="火砲三\n     警告有震波";
			style.normal.textColor = Color.red;
	}
	else if(controlBullet==3){
			bulletPrefab=bulletPrefab1;
			bulletKind="火砲一";
			style.normal.textColor = Color.white;
	}
}