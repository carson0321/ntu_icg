﻿using UnityEngine;
using System.Collections;

public class span : MonoBehaviour {

	public GameObject fire;
	public GameObject barHealth1;
	public GameObject barHealth2;

	public float f_Time=3.0f;
	public float b1_Time=8.0f;
	public float b2_Time=15.0f;

	public Transform tank_position;

	public Vector3 f_V3_Random;
	public Vector3 b1_V3_Random;
	public Vector3 b2_V3_Random;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (f_Time > 0) {f_Time -= Time.deltaTime;} else{GameObject Obj_Clone = Instantiate (fire, f_V3_Random, Quaternion.identity) as GameObject;f_Time=3.0f;}
		if (b1_Time > 0) {b1_Time -= Time.deltaTime;} else{GameObject Obj_Clone1 = Instantiate (barHealth1, b1_V3_Random, Quaternion.identity) as GameObject;b1_Time=8.0f;}
		if (b2_Time > 0) {b2_Time -= Time.deltaTime;} else{GameObject Obj_Clone2 = Instantiate (barHealth2, b2_V3_Random, Quaternion.identity) as GameObject;b2_Time=15.0f;}

		f_V3_Random=new Vector3(Random.Range(tank_position.position.x-8f,tank_position.position.x+8f),
		                      Random.Range(tank_position.position.y,tank_position.position.y+1f),
		                      Random.Range(tank_position.position.z-8f,tank_position.position.z+8f));

		b1_V3_Random=new Vector3(Random.Range(tank_position.position.x-10f,tank_position.position.x+10f),
		                        Random.Range(tank_position.position.y,tank_position.position.y+1f),
		                        Random.Range(tank_position.position.z-10f,tank_position.position.z+10f));

		b2_V3_Random=new Vector3(Random.Range(tank_position.position.x-20f,tank_position.position.x+20f),
		                        Random.Range(tank_position.position.y,tank_position.position.y+1f),
		                        Random.Range(tank_position.position.z-20f,tank_position.position.z+20f));
	}
}
