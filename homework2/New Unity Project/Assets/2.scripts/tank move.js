#pragma strict

var mSpeed : float = 5.0f; //移動速度
var rSpeed : float = 20.0f; //旋轉速度


function Update () {
	var h = Input.GetAxis("Horizontal");//獲取水平軸向按鍵
	var v = Input.GetAxis("Vertical");//獲取垂直軸向按鍵
	
	/*transform.Translate(0,0,mSpeed * -v);//根據水平軸向按鍵來前進或後退
	transform.Rotate(0,rSpeed * h,0);//根據垂直軸向按鍵來旋轉*/
	var move : float = -v * mSpeed * Time.deltaTime;
	transform.Translate(0, 0, move);

	var rotation : float = h * rSpeed * Time.deltaTime;
	transform.Rotate(0, rotation, 0);
}