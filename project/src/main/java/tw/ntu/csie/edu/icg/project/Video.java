package tw.ntu.csie.edu.icg.project;

import static org.bytedeco.javacpp.opencv_core.*;

import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.VideoInputFrameGrabber;
import org.bytedeco.javacpp.opencv_core.IplImage;

import java.awt.image.BufferedImage;

public class Video implements Runnable{
	CanvasFrame originalVideo = new CanvasFrame("The original of webcam");
	public static BufferedImage currentImage;
	
    public void run() {
    	FrameGrabber grabber = new VideoInputFrameGrabber(0);
        try {
        	grabber.start();
        	IplImage videoImage;
        	while (true) {
        		videoImage = grabber.grab();
        		if (videoImage != null) {
        			cvFlip(videoImage, videoImage, 1);// l-r = 90_degrees_steps_anti_clockwise

        			currentImage = videoImage.getBufferedImage();
        			originalVideo.showImage(currentImage);
        			if(!originalVideo.isShowing()){
        				grabber.stop();
        				break;
        			}
                }
        	}
        } catch (Exception e) {
        }
   }
}