package tw.ntu.csie.edu.icg.project;

import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;


@SuppressWarnings("serial")
public class pFrame extends JFrame {
	
	private String filePath,directoryPath,fileNameString =".jpg";;
	public static int imageCount = 0;
	private int radius, intensityLevels;
	
	private JTextField textField,textField_1;
	private JCheckBox chckbxOilRendering,chckbxSketchEffect,chckbxImpressionist,
						chckbxExpressionist,chckbxColoristWash,chckbxPointillist;
	
	private JPanel panel;
	private JScrollPane scrollPane;
	private JLabel originalLabel,oilLabel,sketchLabel,impressionistLabel,expressionistLabel,
					coloristWashLabel,pointillistLabel;
	private Icon icon;
	private BufferedImage bImage;
	private int covertingVideo=0;
	private boolean closeVide=true;
	
	
	public pFrame() throws IOException{
	
		
		this.setTitle("ICG-Project R03944040"); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1024,756);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		
		JButton btnOpenFile = new JButton("Open File");
		btnOpenFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFileAction();
			}
		});
		GridBagConstraints gbc_btnOpenFile = new GridBagConstraints();
		gbc_btnOpenFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnOpenFile.insets = new Insets(0, 0, 5, 5);
		gbc_btnOpenFile.gridx = 0;
		gbc_btnOpenFile.gridy = 0;
		getContentPane().add(btnOpenFile, gbc_btnOpenFile);
		
		JButton btnResetImage = new JButton("Reset Image");
		btnResetImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeImage();
			}
		});
		GridBagConstraints gbc_btnResetImage = new GridBagConstraints();
		gbc_btnResetImage.insets = new Insets(0, 0, 5, 5);
		gbc_btnResetImage.gridx = 1;
		gbc_btnResetImage.gridy = 0;
		getContentPane().add(btnResetImage, gbc_btnResetImage);
		
		JButton btnPlayVideo = new JButton("Play Video");
		btnPlayVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playVideo();
				closeVide=false;
			}
		});
		GridBagConstraints gbc_btnPlayVideo = new GridBagConstraints();
		gbc_btnPlayVideo.insets = new Insets(0, 0, 5, 5);
		gbc_btnPlayVideo.gridx = 2;
		gbc_btnPlayVideo.gridy = 0;
		getContentPane().add(btnPlayVideo, gbc_btnPlayVideo);
		
		JLabel lblRadius = new JLabel("Radius (default:5)");
		GridBagConstraints gbc_lblRadius = new GridBagConstraints();
		gbc_lblRadius.anchor = GridBagConstraints.EAST;
		gbc_lblRadius.insets = new Insets(0, 0, 5, 5);
		gbc_lblRadius.gridx = 3;
		gbc_lblRadius.gridy = 0;
		getContentPane().add(lblRadius, gbc_lblRadius);
		lblRadius.setForeground(Color.blue);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.gridx = 4;
		gbc_textField_2.gridy = 0;
		getContentPane().add(textField, gbc_textField_2);
		textField.setColumns(10);
		
		JLabel lblIntensity = new JLabel("Intensity (default:20)");
		GridBagConstraints gbc_lblIntensity = new GridBagConstraints();
		gbc_lblIntensity.anchor = GridBagConstraints.EAST;
		gbc_lblIntensity.insets = new Insets(0, 0, 5, 5);
		gbc_lblIntensity.gridx = 5;
		gbc_lblIntensity.gridy = 0;
		getContentPane().add(lblIntensity, gbc_lblIntensity);
		lblIntensity.setForeground(Color.blue);
		
		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.gridx = 6;
		gbc_textField_3.gridy = 0;
		getContentPane().add(textField_1, gbc_textField_3);
		textField_1.setColumns(10);
		
		JButton btnInputValue = new JButton("Output Result");
		btnInputValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeVide=true;
				valueChanged();
				imageCoverting();
			}
		});
		GridBagConstraints gbc_btnInputValue_1 = new GridBagConstraints();
		gbc_btnInputValue_1.insets = new Insets(0, 0, 5, 0);
		gbc_btnInputValue_1.gridx = 7;
		gbc_btnInputValue_1.gridy = 0;
		getContentPane().add(btnInputValue, gbc_btnInputValue_1);
		
		ButtonGroup checkGroup = new ButtonGroup();
		chckbxOilRendering = new JCheckBox("Basic oil painting");
		GridBagConstraints gbc_chckbxOilRendering = new GridBagConstraints();
		gbc_chckbxOilRendering.anchor = GridBagConstraints.WEST;
		gbc_chckbxOilRendering.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxOilRendering.gridx = 0;
		gbc_chckbxOilRendering.gridy = 1;
		getContentPane().add(chckbxOilRendering, gbc_chckbxOilRendering);
		chckbxOilRendering.setForeground(Color.blue);
		chckbxOilRendering.setSelected(true);
		checkGroup.add(chckbxOilRendering);
		
		chckbxSketchEffect = new JCheckBox("Sketch effect");
		GridBagConstraints gbc_chckbxSketchEffect = new GridBagConstraints();
		gbc_chckbxSketchEffect.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxSketchEffect.anchor = GridBagConstraints.WEST;
		gbc_chckbxSketchEffect.gridx = 1;
		gbc_chckbxSketchEffect.gridy = 1;
		getContentPane().add(chckbxSketchEffect, gbc_chckbxSketchEffect);
		chckbxSketchEffect.setForeground(Color.blue);
		checkGroup.add(chckbxSketchEffect);
		
		JLabel lblAdvancedRendering = new JLabel("Advanced rendering");
		GridBagConstraints gbc_chckbxAdvancedRendering = new GridBagConstraints();
		gbc_chckbxAdvancedRendering.anchor = GridBagConstraints.WEST;
		gbc_chckbxAdvancedRendering.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxAdvancedRendering.gridx = 2;
		gbc_chckbxAdvancedRendering.gridy = 1;
		getContentPane().add(lblAdvancedRendering, gbc_chckbxAdvancedRendering);
		lblAdvancedRendering.setForeground(Color.red);
		
		chckbxImpressionist = new JCheckBox("Impressionist");
		GridBagConstraints gbc_chckbxImpressionist = new GridBagConstraints();
		gbc_chckbxImpressionist.anchor = GridBagConstraints.WEST;
		gbc_chckbxImpressionist.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxImpressionist.gridx = 3;
		gbc_chckbxImpressionist.gridy = 1;
		getContentPane().add(chckbxImpressionist, gbc_chckbxImpressionist);
		chckbxImpressionist.setForeground(Color.red);
		checkGroup.add(chckbxImpressionist);
		
		chckbxExpressionist = new JCheckBox("Expressionist");
		GridBagConstraints gbc_chckbxExpressionist = new GridBagConstraints();
		gbc_chckbxExpressionist.anchor = GridBagConstraints.WEST;
		gbc_chckbxExpressionist.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxExpressionist.gridx = 4;
		gbc_chckbxExpressionist.gridy = 1;
		getContentPane().add(chckbxExpressionist, gbc_chckbxExpressionist);
		chckbxExpressionist.setForeground(Color.red);
		checkGroup.add(chckbxExpressionist);
		
		chckbxColoristWash = new JCheckBox("Colorist Wash");
		GridBagConstraints gbc_chckbxColoristWash = new GridBagConstraints();
		gbc_chckbxColoristWash.anchor = GridBagConstraints.WEST;
		gbc_chckbxColoristWash.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxColoristWash.gridx = 5;
		gbc_chckbxColoristWash.gridy = 1;
		getContentPane().add(chckbxColoristWash, gbc_chckbxColoristWash);
		chckbxColoristWash.setForeground(Color.red);
		checkGroup.add(chckbxColoristWash);
		
		chckbxPointillist = new JCheckBox("Pointillist");
		GridBagConstraints gbc_chckbxPointillist = new GridBagConstraints();
		gbc_chckbxPointillist.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxPointillist.anchor = GridBagConstraints.WEST;
		gbc_chckbxPointillist.gridx = 6;
		gbc_chckbxPointillist.gridy = 1;
		getContentPane().add(chckbxPointillist, gbc_chckbxPointillist);
		chckbxPointillist.setForeground(Color.red);
		checkGroup.add(chckbxPointillist);
		
		
		panel = new JPanel();
		scrollPane = new JScrollPane(panel);
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridheight = 6;
		gbc_scrollPane_1.gridwidth = 8;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 2;
		getContentPane().add(scrollPane, gbc_scrollPane_1);
		
        
		this.setVisible(true);
//		this.setResizable(false);
		
//		BufferedImage myPicture = ImageIO.read(new File("image/1.jpg"));
//		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
//        this.add(picLabel);
		
	}
	
    public  void btnFileAction() {
        FileDialog fd = new FileDialog(this, "", FileDialog.LOAD);
        fd.setFile("*.jpg;*.jpeg;*.png");
        fd.setVisible(true);
        String file = fd.getFile();
        if (file == null)
            return;
        directoryPath = fd.getDirectory();
        filePath = directoryPath + file;
        if(file.endsWith(".jpg") || file.endsWith(".jpeg")) fileNameString=".jpg";
        else if(file.endsWith(".png")) fileNameString=".png";
        
        ImageIcon image = new ImageIcon(filePath);
        originalLabel = new JLabel(image);
        panel.add(originalLabel);
        icon = originalLabel.getIcon();
        bImage = new BufferedImage(
         		icon.getIconWidth(),icon.getIconHeight(),BufferedImage.TYPE_3BYTE_BGR);
         Graphics Graphic = bImage.createGraphics();
         icon.paintIcon(null, Graphic, 0,0);
         Graphic.dispose();
         this.setVisible(true);
    }
    public  void valueChanged(){
		if(textField.getText().isEmpty()) radius=5;
		else radius=Integer.valueOf(textField.getText());
		if(textField_1.getText().isEmpty()) intensityLevels=20;
		else	intensityLevels=Integer.valueOf(textField_1.getText());
		if(chckbxOilRendering.isSelected()) JOptionPane.showMessageDialog(null, "Radius:" + radius + "\nintensity" + intensityLevels ,"Value Changed", JOptionPane.INFORMATION_MESSAGE); 
	}
    
    public void imageCoverting() {
         if(chckbxOilRendering.isSelected() && closeVide){
         	ImageIcon oilImage = new ImageIcon( ProduceImage.OilImage(directoryPath, fileNameString, bImage, radius, intensityLevels));
        	oilLabel = new JLabel(oilImage);
            panel.add(oilLabel);
            imageCount++;
         }
         
         if(chckbxSketchEffect.isSelected() && closeVide){
             ImageIcon sketchImage = new ImageIcon( ProduceImage.SketchImage(directoryPath, fileNameString, bImage));
             sketchLabel = new JLabel(sketchImage);
             panel.add(sketchLabel);
             imageCount++;
         }
         if(chckbxImpressionist.isSelected() && closeVide){
        	 ProduceImage.doPainterly(bImage, 1.0f, 1.0f, 100.0f, 16, 4, 8, 255, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
        	 ImageIcon impressionistImage = new ImageIcon(ProduceImage.getTargetImage());
        	 ProduceImage.saveAdvancedImage(directoryPath, fileNameString,ProduceImage.getTargetImage());
        	 impressionistLabel = new JLabel(impressionistImage);
        	 panel.add(impressionistLabel);
        	 imageCount++;
         }
         if(chckbxExpressionist.isSelected() && closeVide){
        	 ProduceImage.doPainterly(bImage, 1.0f, 0.25f, 50.0f, 16, 10, 8, 192, 0.0f, 0.0f, 0.7f, 0.0f, 0.0f, 0.0f);
        	 ImageIcon expressionistImage = new ImageIcon(ProduceImage.getTargetImage());
        	 ProduceImage.saveAdvancedImage(directoryPath, fileNameString,ProduceImage.getTargetImage());
        	 expressionistLabel = new JLabel(expressionistImage);
        	 panel.add(expressionistLabel);
        	 imageCount++;
         }
         if(chckbxColoristWash.isSelected() && closeVide){
        	 ProduceImage.doPainterly(bImage, 1.0f, 1.0f, 200.0f, 16, 4, 8, 128, 0.0f, 0.0f, 0.0f, 0.3f, 0.3f, 0.3f);
        	 ImageIcon coloristWashImage = new ImageIcon(ProduceImage.getTargetImage());
        	 ProduceImage.saveAdvancedImage(directoryPath, fileNameString,ProduceImage.getTargetImage());
        	 coloristWashLabel = new JLabel(coloristWashImage);
        	 panel.add(coloristWashLabel);
        	 imageCount++;
         }
         if(chckbxPointillist.isSelected() && closeVide){
        	 ProduceImage.doPainterly(bImage, 0.5f, 1.0f, 100.0f, 0, 0, 4, 255, 0.3f, 0.0f, 0.99f, 0.0f, 0.0f, 0.0f);
        	 ImageIcon pointillistImage = new ImageIcon(ProduceImage.getTargetImage());
        	 ProduceImage.saveAdvancedImage(directoryPath, fileNameString,ProduceImage.getTargetImage());
        	 pointillistLabel = new JLabel(pointillistImage);
        	 panel.add(pointillistLabel);
        	 imageCount++;
         }
         this.setVisible(true);
	}
    
    public void removeImage(){
    	panel.removeAll();
    	panel.repaint();
    	panel.revalidate();
    	this.setVisible(true);
    }
    public static int covertingCount=0;
    public void playVideo(){
    	valueChanged();
    	if(chckbxOilRendering.isSelected()) covertingVideo=1;
    	if(chckbxSketchEffect.isSelected()) covertingVideo=2;
    	if(chckbxImpressionist.isSelected()) covertingVideo=3;
    	if(chckbxExpressionist.isSelected()) covertingVideo=4;
    	if(chckbxColoristWash.isSelected()) covertingVideo=5;
    	if(chckbxPointillist.isSelected()) covertingVideo=6;
    	Video runVideo = new Video();
    	Thread startVideo = new Thread(runVideo);
    	startVideo.start();
    	
    	Effect runEffect = new Effect(covertingVideo, radius, intensityLevels);
    	Thread startEffect = new Thread(runEffect);
		startEffect.start();
    	
    }
    
	public static void main( String[] args ) throws IOException {
		new pFrame();
	}
	public static class SobelVector {
		public short x;
		public short y;
	};
}