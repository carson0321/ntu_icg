package tw.ntu.csie.edu.icg.project;

import java.awt.image.BufferedImage;

import org.bytedeco.javacv.CanvasFrame;

public class Effect implements Runnable{
	CanvasFrame effectVideo = new CanvasFrame("The effective webcam");
	private int type,radius, intensityLevels;
	
	public Effect(int type,int radius,int intensityLevels) {
		this.type=type;
		this.radius=radius;
		this.intensityLevels=intensityLevels;
	}
	
	public void run() {
		 try {
			    Thread.sleep(50);
	        	while (true) {
	        		if (Video.currentImage != null) {
	        			if(type==1){
	        				BufferedImage resultFrame = ProduceImage.OilVideo(Video.currentImage,radius,intensityLevels);
	        				effectVideo.showImage(resultFrame);
	        			}
	        			else if(type==2){
	        				BufferedImage resultFrame = ProduceImage.SketchVideo(Video.currentImage);
	        				effectVideo.showImage(resultFrame);
	        			}
	        			else if(type==3){
	        				ProduceImage.doPainterly(Video.currentImage, 1.0f, 1.0f, 100.0f, 16, 4, 8, 255, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
//	        	        	BufferedImage resultFrame = ProduceImage.getTargetImage();
//	        	        	resultVideoImage = IplImage.createFrom(resultFrame);
	        				effectVideo.showImage(ProduceImage.getTargetImage());
	        			}
	        			else if(type==4){
	        				ProduceImage.doPainterly(Video.currentImage, 1.0f, 0.25f, 50.0f, 16, 10, 8, 192, 0.0f, 0.0f, 0.7f, 0.0f, 0.0f, 0.0f);
//	        	        	BufferedImage resultFrame = ProduceImage.getTargetImage();
//	        	        	resultVideoImage = IplImage.createFrom(resultFrame);
	        				effectVideo.showImage(ProduceImage.getTargetImage());
	        			}
	        			else if(type==5){
	        				ProduceImage.doPainterly(Video.currentImage, 1.0f, 1.0f, 200.0f, 16, 4, 8, 128, 0.0f, 0.0f, 0.0f, 0.3f, 0.3f, 0.3f);
//	        	        	BufferedImage resultFrame = ProduceImage.getTargetImage();
//	        	        	resultVideoImage = IplImage.createFrom(resultFrame);
	        				effectVideo.showImage(ProduceImage.getTargetImage());
	        			}
	        			else if(type==6){
	        				ProduceImage.doPainterly(Video.currentImage, 0.5f, 1.0f, 100.0f, 0, 0, 4, 255, 0.3f, 0.0f, 0.99f, 0.0f, 0.0f, 0.0f);
//	        	        	BufferedImage resultFrame = ProduceImage.getTargetImage();
//	        	        	resultVideoImage = IplImage.createFrom(resultFrame);
	        				effectVideo.showImage(ProduceImage.getTargetImage());
	        			}
	                }
	        		if(!effectVideo.isShowing()) break;
	        		Thread.sleep(50);
	        	}
	        } catch (Exception e) {
	        }
	}

}
