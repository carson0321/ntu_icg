package tw.ntu.csie.edu.icg.project;


//import java.awt.Color;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Vector;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import tw.ntu.csie.edu.icg.project.pFrame.SobelVector;
import com.jhlabs.image.GaussianFilter;
//import static org.bytedeco.javacpp.opencv_core.*;
//import static org.bytedeco.javacpp.opencv_imgproc.*;
//import static org.bytedeco.javacpp.opencv_highgui.*;


public class ProduceImage {
	
	public static BufferedImage SketchImage(String directoryPath, String fileNameString ,BufferedImage sourceImage) {
		byte[] pixels = ((DataBufferByte) sourceImage.getRaster().getDataBuffer())
	            .getData();
		int width = sourceImage.getWidth();
		int height = sourceImage.getHeight();
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		Mat src = new Mat(height,width, CvType.CV_8UC3);
		src.put(0,0,pixels);
        
        Mat gray0 = new Mat(src.size(),CvType.CV_8UC1);
        Mat gray1 = new Mat(src.size(),CvType.CV_8UC1);
        Mat NULL = new Mat(src.size(),CvType.CV_8UC1);
        
        Imgproc.cvtColor(src,gray0,Imgproc.COLOR_RGB2GRAY);
        
        Core.addWeighted(gray0,-1,NULL,0,255,gray1);
        
        Size size= new Size(11,11);
        Imgproc.GaussianBlur(gray1,gray1,size,0);
        
        Mat img = new Mat(src.size(),CvType.CV_8UC1);
        for(int y = 0; y < src.rows(); y++){
        	for(int x = 0; x < src.cols(); x++){
        		double[] p0 = gray0.get(y, x);
        		double[] p1 = gray1.get(y, x);
        		double[] p = img.get(y, x);
        		
        		for(int i = 0; i < p.length;i++)
        			p[i]= Math.min((p0[i]+(p0[i]*p1[i])/(255-p1[i])), 255);
        		img.put(y, x, p);
        	}
        }
        MatOfByte bytemat = new MatOfByte();
        Highgui.imencode(".jpg", img, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage result = null;
        try {  
        	result = ImageIO.read(in);  
        } catch (IOException e) {  
             e.printStackTrace();  
        }
        String format = null;
		if(fileNameString.endsWith(".jpg") || fileNameString.endsWith("jpeg")) format="jpg";
        else if(fileNameString.endsWith(".png")) format="png";
		String count = directoryPath + "SketchImage" +
        		Integer.toString(pFrame.imageCount) + fileNameString;
		try {
			ImageIO.write(result, format,new File(count));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
//	public static BufferedImage SketchImage(String directoryPath, String fileNameString ,BufferedImage sourceImage) {
//		
//		int width = sourceImage.getWidth();
//		int height = sourceImage.getHeight();
//		IplImage srcImage = IplImage.create(sourceImage.getWidth(),sourceImage.getHeight(),IPL_DEPTH_8U,1);
//		srcImage = IplImage.createFrom(sourceImage);
//		
//		IplImage gray = cvCreateImage(cvGetSize(srcImage), IPL_DEPTH_8U, 1);
//		IplImage gaussian = cvCreateImage(cvGetSize(srcImage), IPL_DEPTH_8U, 1); 
//	
//		cvCvtColor( srcImage, gray, CV_BGR2GRAY );
//		cvNot( gray, gaussian);
//	
//		cvSmooth(gaussian, gaussian, CV_GAUSSIAN, 11, 11, 0, 0);
//	
//		BufferedImage aImg = gray.getBufferedImage();
//		BufferedImage bImg = gaussian.getBufferedImage();
//		int [] aImgData = aImg.getRGB(0, 0, width, height, null, 0, width); 
//		int [] bImgData = bImg.getRGB(0, 0, width, height, null, 0, width);
//		int [] resultImgData = new int[width*height];
//	
//		for(int y = 0; y < height; y++){
//			for(int x = 0; x < width; x++){
//				double a = aImgData[x+y*width];
//				double b = bImgData[x+y*width];
//				resultImgData[x+y*width] = (int) Math.min((a + (a*b)/(255-b)),255);
//			}
//		}
//		bImg.setRGB( 0, 0, width, height, resultImgData, 0, width);
//		return bImg;
//	}
	
	public static BufferedImage SketchVideo(BufferedImage sourceImage) {
		byte[] pixels = ((DataBufferByte) sourceImage.getRaster().getDataBuffer())
	            .getData();
		int width = sourceImage.getWidth();
		int height = sourceImage.getHeight();
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		Mat src = new Mat(height,width, CvType.CV_8UC3);
		src.put(0,0,pixels);
		
	    Mat gray0 = new Mat(src.size(),CvType.CV_8UC1);
        Mat gray1 = new Mat(src.size(),CvType.CV_8UC1);
        Mat NULL = new Mat(src.size(),CvType.CV_8UC1);
        
        Imgproc.cvtColor(src,gray0,Imgproc.COLOR_RGB2GRAY);
        
        Core.addWeighted(gray0,-1,NULL,0,255,gray1);
        
        Size size= new Size(11,11);
        Imgproc.GaussianBlur(gray1,gray1,size,0);
        
        Mat img = new Mat(src.size(),CvType.CV_8UC1);
        for(int y = 0; y < src.rows(); y++){
        	for(int x = 0; x < src.cols(); x++){
        		double[] p0 = gray0.get(y, x);
        		double[] p1 = gray1.get(y, x);
        		double[] p = img.get(y, x);
        		
        		for(int i = 0; i < p.length;i++)
        			p[i]= Math.min((p0[i]+(p0[i]*p1[i])/(255-p1[i])), 255);
        		img.put(y, x, p);
        	}
        }
        
        MatOfByte bytemat = new MatOfByte();
        Highgui.imencode(".jpg", img, bytemat);
        byte[] bytes = bytemat.toArray();
        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage result = null;
        try {  
        	result = ImageIO.read(in);  
        } catch (IOException e) {  
             e.printStackTrace();  
        }
		return result;
	}
	public static BufferedImage OilImage(String directoryPath, String fileNameString, BufferedImage sourceImage,int radius,int intensityLevels){		
		BufferedImage destinationImage=new BufferedImage(
				sourceImage.getWidth(),
				sourceImage.getHeight(),
				sourceImage.getType());
		int averageR[] = new int[intensityLevels];
		int averageG[]=new int[intensityLevels];
		int averageB[]=new int[intensityLevels];
		int intensityCount[]=new int[intensityLevels];
		for(int x=0;x< sourceImage.getWidth();x++){
			int left = Math.max(0,x-radius);
			int right = Math.min(x+radius,destinationImage.getWidth()-1);
			for(int y=0;y< sourceImage.getHeight();y++){
				int top = Math.max(0,y-radius);
				int bottom = Math.min(y+radius,destinationImage.getHeight()-1);
				Arrays.fill(averageR,0);
				Arrays.fill(averageG,0);
				Arrays.fill(averageB,0);
				Arrays.fill(intensityCount,0);
				int maxIndex=-1;
				for(int j=top;j<=bottom;j++){
					for(int i=left;i<=right;i++){
						if(!inRange(x,y,i, j,radius)) continue;
						int rgb = sourceImage.getRGB(i,j);
						int red = (rgb >> 16)&0xFF;
						int green = (rgb >>8)&0xFF;
						int blue = (rgb )&0xFF;
						int intensityIndex = (int)((((red+green+blue)/3.0)/256.0)*intensityLevels);
						intensityCount[intensityIndex]++;
						averageR[intensityIndex] += red;
						averageG[intensityIndex] += green;
						averageB[intensityIndex] += blue;
						if( maxIndex==-1 || intensityCount[maxIndex]< intensityCount[intensityIndex]){
							maxIndex = intensityIndex;
						}
					}
				}
				int curMax = intensityCount[maxIndex];
				int r = averageR[maxIndex] / curMax;
				int g = averageG[maxIndex] / curMax;
				int b = averageB[maxIndex] / curMax;
				int rgb=((r << 16) | ((g << 8) | b));
				destinationImage.setRGB(x,y,rgb);
			}
		}
		String format = null;
		if(fileNameString.endsWith(".jpg") || fileNameString.endsWith("jpeg")) format="jpg";
        else if(fileNameString.endsWith(".png")) format="png";
		String count = directoryPath + "OilImage" +
        		Integer.toString(pFrame.imageCount) + fileNameString;
		try {
			ImageIO.write(destinationImage, format,new File(count));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return destinationImage;
	}
	public static BufferedImage OilVideo(BufferedImage sourceImage,int radius,int intensityLevels){		
		BufferedImage destinationImage=new BufferedImage(
				sourceImage.getWidth(),
				sourceImage.getHeight(),
				sourceImage.getType());
		int averageR[] = new int[intensityLevels];
		int averageG[]=new int[intensityLevels];
		int averageB[]=new int[intensityLevels];
		int intensityCount[]=new int[intensityLevels];
		for(int x=0;x< sourceImage.getWidth();x++){
			int left = Math.max(0,x-radius);
			int right = Math.min(x+radius,destinationImage.getWidth()-1);
			for(int y=0;y< sourceImage.getHeight();y++){
				int top = Math.max(0,y-radius);
				int bottom = Math.min(y+radius,destinationImage.getHeight()-1);
				Arrays.fill(averageR,0);
				Arrays.fill(averageG,0);
				Arrays.fill(averageB,0);
				Arrays.fill(intensityCount,0);
				int maxIndex=-1;
				for(int j=top;j<=bottom;j++){
					for(int i=left;i<=right;i++){
						if(!inRange(x,y,i, j,radius)) continue;
						int rgb = sourceImage.getRGB(i,j);
						int red = (rgb >> 16)&0xFF;
						int green = (rgb >>8)&0xFF;
						int blue = (rgb )&0xFF;
						int intensityIndex = (int)((((red+green+blue)/3.0)/256.0)*intensityLevels);
						intensityCount[intensityIndex]++;
						averageR[intensityIndex] += red;
						averageG[intensityIndex] += green;
						averageB[intensityIndex] += blue;
						if( maxIndex==-1 || intensityCount[maxIndex]< intensityCount[intensityIndex]){
							maxIndex = intensityIndex;
						}
					}
				}
				int curMax = intensityCount[maxIndex];
				int r = averageR[maxIndex] / curMax;
				int g = averageG[maxIndex] / curMax;
				int b = averageB[maxIndex] / curMax;
				int rgb=((r << 16) | ((g << 8) | b));
				destinationImage.setRGB(x,y,rgb);
			}
		}
		return destinationImage;
	}
	private static boolean inRange(int cx,int cy,int i,int j,int radius){
		double d;
		d=Point2D.distance(i, j,cx,cy);
		return d<radius;
	}
	
	public static BufferedImage getTargetImage() {
		return target;		
	}
	
    public static float gridSize,curvatureFilter,threshold;
	public static int maxStrokeLength,minStrokeLength,maxBrushSize,colorOpacity;
	public static float hueJitter,saturationJitter,valueJitter,redJitter,greenJitter,blueJitter;

	
	public final static float[] sobelXMatrix = { 
		-1.0f, 0.0f, 1.0f,
		-2.0f, 0.0f , 2.0f,
		-1.0f, 0.0f, 1.0f };
	public final static float[] sobelYMatrix = {
		-1.0f, -2.0f, -1.0f,
		0f, 0f, 0f,
		1.0f, 2.0f, 1.0f
	};
	private static BufferedImage source;
	private static BufferedImage target;
	private static BufferedImage writtenArea;
	private static BufferedImage blurred = null;	
	private static SobelVector[] sobelVector = null;
	
    public static void saveAdvancedImage(String directoryPath, String fileNameString, BufferedImage sourceImage){
    	String format = null;
		if(fileNameString.endsWith(".jpg") || fileNameString.endsWith("jpeg")) format="jpg";
        else if(fileNameString.endsWith(".png")) format="png";
		String count = directoryPath + "AdvancedImage" +
        		Integer.toString(pFrame.imageCount) + fileNameString;
		try {
			ImageIO.write(sourceImage, format,new File(count));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
	
	private static short doConvolution( int x, int y, BufferedImage source, float[] convolve ) {
		short value = 0;
		for ( int i = 0; i < 3; i++ ) {
			for ( int j = 0; j < 3; j++ ) {
				try {
					int intensity = new Color( source.getRGB( x + i - 1, y + j - 1 ) ).getBlue();
					value += intensity * convolve[j*3+i];
				} catch ( Exception e ) {
				}
			}
		}
		return value;
	}
	
	private static void calculateSobelVectors( BufferedImage luma ) {
		int width = luma.getWidth();
		int height = luma.getHeight();
		
		for ( int y = 0; y < height; ++y ) {
			for ( int x = 0; x < width; ++x ) {
				SobelVector v = sobelVector[y*width+x];
				v.x = doConvolution( x, y, luma, sobelXMatrix );
				v.y = doConvolution( x, y, luma, sobelYMatrix );
			}
		}
	}
	
	public static void doPainterly(BufferedImage src, float _gridSize, float _curvatureFilter, float _threshold, 
			int _maxStrokeLength, int _minStrokeLength, int _maxBrushSize, int _colorOpacity,
			float _hueJitter, float _saturationJitter, float _valueJitter,
			float _redJitter, float _greenJitter, float _blueJitter) {
		source=src;
    	gridSize = _gridSize;
    	curvatureFilter = _curvatureFilter;
		threshold = _threshold;
		maxStrokeLength = _maxStrokeLength;
		minStrokeLength = _minStrokeLength;
		maxBrushSize = _maxBrushSize;
		colorOpacity = _colorOpacity;
		hueJitter = _hueJitter;
		saturationJitter = _saturationJitter;
		valueJitter = _valueJitter;
		redJitter = _redJitter;
		greenJitter = _greenJitter;
		blueJitter = _blueJitter;
		target = new BufferedImage( source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_ARGB );
		writtenArea = new BufferedImage( source.getWidth(), source.getHeight(), BufferedImage.TYPE_BYTE_BINARY );
		sobelVector = new SobelVector[source.getWidth() * source.getHeight()];
		for ( int i = 0; i < sobelVector.length; i++ ) {
			sobelVector[i] = new SobelVector();
		}
		try {
			{
				Graphics2D g = target.createGraphics();
				g = target.createGraphics();
				g.setBackground( Color.white );
				g.clearRect( 0, 0, target.getWidth(), target.getHeight() );
			}
			{
				Graphics2D g = writtenArea.createGraphics();
				g = writtenArea.createGraphics();
				g.setBackground( Color.black );
				g.clearRect( 0, 0, writtenArea.getWidth(), writtenArea.getHeight() );
			}
			for ( int i = maxBrushSize; i > 1; i/=2 ) {
				BufferedImageOp gaussian = new GaussianFilter((float)(i*0.5));
				blurred = gaussian.filter( source, null );
				BufferedImage luma = new BufferedImage( blurred.getWidth(), blurred.getHeight(), BufferedImage.TYPE_BYTE_GRAY );
				luma.createGraphics().drawImage( blurred, 0, 0, null );
				calculateSobelVectors( luma );
				paintLayer( (float)i );
			}
		} catch ( Exception e ) {
			Graphics2D g = target.createGraphics();
			g = target.createGraphics();
			g.setBackground( new Color( 255, 255, 255 ) );
			g.clearRect( 0, 0, target.getWidth(), target.getHeight() );
		}
	}
	
	private static void paintLayer( float brushSize ) {
		int grid = Math.round( gridSize * brushSize );
		int gridsqr = grid * grid;
		
		Vector<Point2D.Float> strokeList = new Vector<Point2D.Float>();
		
		for ( int x = 0; x < source.getWidth(); x += grid ) {
			for ( int y = 0; y < source.getHeight(); y+= grid ) {
				float areaError = 0.0f;
				float worstError = Float.MIN_VALUE;
				int wEX = x, wEY = y;
				
				for ( int eY = y - grid/2; eY < y + grid/2; eY++ ) {
					for ( int eX = x - grid/2; eX < x + grid/2; eX++  ) {
						int jitteredX = eX + (int)((Math.random() - 0.5) * grid );
						int jitteredY = eY + (int)((Math.random() - 0.5) * grid );
		
						if ( jitteredX < 0 ) { jitteredX = 0; }
						if ( jitteredX >= source.getWidth() ) { jitteredX = source.getWidth() - 1; }
						
						if ( jitteredY < 0 ) { jitteredY = 0; }
						if ( jitteredY >= source.getHeight() ) { jitteredY = source.getHeight() - 1; }
						
						float difference = getDifference( jitteredX, jitteredY );
						if ( difference > worstError ) {
							worstError = difference;
							wEX = jitteredX;
							wEY = jitteredY;
						}
						areaError += difference;
					}
				}
				
				areaError /= (float)gridsqr;
				if ( areaError > threshold ) {
					strokeList.add( new Point2D.Float( (float)wEX, (float)wEY ) );
				}
			}
		}
		
		while ( !strokeList.isEmpty() ) {
			Point2D.Float obj = strokeList.remove( (int)Math.floor( Math.random() * strokeList.size() ) );
			makeStroke( brushSize, obj );
		}
	}

	static float clamp( float v, float l, float h ) {
		if ( v >= h ) { v = h; }
		if ( v <= l ) { v = l; }
		
		return v;
	}
	static int clamp( int v, int l, int h ) {
		if ( v >= h ) { v = h; }
		if ( v <= l ) { v = l; }
		
		return v;
	}
	
	static Color createStrokeColor( int x, int y ) {
		Color temp = new Color( source.getRGB( x, y ), true );
		
		if ( doDrawDot ) {
			Color baseStrokeColor = new Color( temp.getRed(), temp.getGreen(), temp.getBlue() );
	
			float[] hsbvals = new float[3];
			Color.RGBtoHSB(baseStrokeColor.getRed(), baseStrokeColor.getGreen(), baseStrokeColor.getBlue(), hsbvals);
			hsbvals[0] += ( Math.random() - 0.5f ) * hueJitter;
			hsbvals[1] += ( Math.random() - 0.5f ) * saturationJitter;
			hsbvals[2] += ( Math.random() - 0.5f ) * valueJitter;
			
			Color jitteredHSB = new Color( Color.HSBtoRGB( 
					clamp( hsbvals[0], 0.0f, 1.0f ),
					clamp( hsbvals[1], 0.0f, 1.0f ),
					clamp( hsbvals[2], 0.0f, 1.0f ) ) );
			Color jitteredRGB = new Color(
					clamp( jitteredHSB.getRed() + (int)(( Math.random() - 0.5f ) * redJitter), 0, 255 ),
					clamp( jitteredHSB.getGreen() + (int)(( Math.random() - 0.5f ) * greenJitter), 0, 255 ),
					clamp( jitteredHSB.getBlue() + (int)(( Math.random() - 0.5f ) * blueJitter), 0, 255 ),
					colorOpacity );
			
			return jitteredRGB;
		} else {
			return temp;
		}
	}
	
	private static boolean doDrawDot = true;
	
	static void renderStroke( BufferedImage t, Vector<Point2D.Float> path, Color strokeColor, float brushSize ) {
		Point2D.Float initial = path.elementAt( 0 );
		
		Graphics2D g = t.createGraphics();
		g = t.createGraphics();

		g.getRenderingHints().put( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
		g.setColor( strokeColor );

		float halfBrush = 0;
		if ( path.size() > 1 ) {
			GeneralPath gp = new GeneralPath();

			gp.moveTo( (int)( initial.x - halfBrush ), (int)( initial.y - halfBrush ) );
			for ( int i = 1; i < path.size() - 1; i++ ) {
				Point2D.Float p1 = path.elementAt( i - 1 );
				Point2D.Float p2 = path.elementAt( i );
				Point2D.Float p3 = path.elementAt( i + 1 );
				
				gp.curveTo( p1.x - halfBrush, p1.y - halfBrush,
						p2.x - halfBrush, p2.y - halfBrush,
						p3.x - halfBrush, p3.y - halfBrush);
			}
			gp.lineTo( path.elementAt( path.size() - 1 ).x - halfBrush, path.elementAt( path.size() - 1 ).y - halfBrush );
			
			g.setStroke( new BasicStroke( brushSize ) );
			g.draw( gp );
		} else {
			if ( doDrawDot ) {
				g.fillOval( (int)( initial.x - halfBrush ), (int)( initial.y - halfBrush ), 
						(int)brushSize, (int)brushSize );
			}
		}
	}
	
	static void makeStroke( float brushSize, Point2D.Float initial ) {
		Vector<Point2D.Float> path = new Vector<Point2D.Float>();
		
		Point2D.Float current = initial;
		Point2D.Float lastDelta = new Point2D.Float( 0.0f, 0.0f );
		Color strokeColor = createStrokeColor( (int)current.getX(), (int)current.getY() );

		path.add( (Point2D.Float)current.clone() );
		for ( int i = 1; i < maxStrokeLength; i++ ) {
			if ( i > minStrokeLength && 
					getDifference( (int)current.x, (int)current.y ) < getDifference( new Color( source.getRGB( (int)current.getX(), (int)current.getY() ), true ), strokeColor ) ) {
				break;
			}
			
			if ( getGradientMagnitude( current ) == 0.0f ) {
				break;
			}
			
			Point2D.Float gradient = getGradientDirection( current );
			Point2D.Float delta = new Point2D.Float( (float)-gradient.getY(), (float)gradient.getX() );
			
			if ( lastDelta.getX() * delta.getX() + lastDelta.getY() * delta.getY() < 0 ) {
				delta.x = -delta.x;
				delta.y = -delta.y;
			}
			
			float deltaMag = (float)Math.sqrt( delta.x * delta.x + delta.y * delta.y );
			delta.x = curvatureFilter * delta.x + ( 1 - curvatureFilter ) * lastDelta.x;
			delta.x = delta.x / deltaMag;
			
			delta.y = curvatureFilter * delta.y + ( 1 - curvatureFilter ) * lastDelta.y;
			delta.y = delta.y / deltaMag;

			current.x += brushSize * delta.x;
			current.y += brushSize * delta.y;
			
			if ( current.x < 0.0f || current.x >= source.getWidth() ||
					current.y < 0.0f || current.y >= source.getHeight() ) {
				break;
			}
			
			lastDelta = delta;	

			path.add( (Point2D.Float)current.clone() );
		}
		
		renderStroke( target, path, strokeColor, brushSize );
		renderStroke( writtenArea, path, Color.white, brushSize );
	}
	
	private static float getDifference( Color sC, Color tC ) {
		int dR = ( sC.getRed() - tC.getRed() );
		int dB = ( sC.getBlue() - tC.getBlue() );
		int dG = ( sC.getGreen() - tC.getGreen() );
	
		return (float)Math.sqrt( ( double )( dR * dR + dB * dB + dG * dG ) );
	}
	
	private static float getDifference( int eX, int eY ) {
		float difference;
		
		Color sC = new Color( source.getRGB( eX, eY ), true );
		Color tC = new Color( target.getRGB( eX, eY ), true );
		Color wC = new Color( writtenArea.getRGB( eX, eY ) );
		
		if ( wC.equals( Color.black ) ) {
			difference = (float)Integer.MAX_VALUE;
		} else {
			difference = getDifference( sC, tC );
		}
		
		return difference;
	}
	
	private static SobelVector getGradientVector( Point2D.Float pos ) {
		return sobelVector[(int)pos.y * source.getWidth() + (int)pos.x];
	}
	
	private static float getGradientMagnitude( Point2D.Float pos ) {
		SobelVector sv = getGradientVector( pos );
		return (float)Math.sqrt( sv.x * sv.x + sv.y * sv.y );
	}
	
	private static Point2D.Float getGradientDirection( Point2D.Float pos ) {
		SobelVector sv = getGradientVector( pos );
		
		Point2D.Float gradient = new Point2D.Float( sv.x, sv.y );
		
		gradient.x /= getGradientMagnitude( pos );
		gradient.y /= getGradientMagnitude( pos );
		
		return gradient;
	}
}
