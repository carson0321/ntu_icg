# NTU_ICG

National Taiwan University 2014 Interactive Computer Graph Course  

# Homeworks
## homework1
Multiple Shading and Transformation.

+ Flat, Gauraud, and Phong shading with Phong illumination model in shaders. You can demonstrate the three shading computation result in a single object (3pts)  
+ Enable multiple shaders and transformations on multiple objects in a scene. You are free to use those provided model files and arrange them to form the scene on your own style. You must show the three shading simultaneously on different objects in your scene (3pts).  
+ Bonus: special effects on animation 

**Programming Language:** C++  
**IDE:** VS2013

## homework2
Create an simple Unity3D Game.

+ Fundamental scene：3pt  
	* Create own game characters in the specified scene 
	* Create script that game characters can be moved in the scene
	* Increase the cameras in the scene, so that the game can be viewed with a different perspective (first-person or third-person perspective switch)
+ Fundamental controller of role：3pt  
	* Create role of weapons, so that users can be fired bullets  
	* Create systems of different bullets. According to different bullets, it has different effects to hit objects
	* Create role of the monster. It can attack the player character, users can destroy it with a weapon
+ Bonus  
	* Any idea can be added to an existing game. For example, design other GUI, provide different scenes, levels, and systems(grading, health bar..etc)

**Programming Language:** C#/JS  
**IDE:** Unity3D

# Term Project Listing

1. Animation of articulated figures (linked)
2. Rigid body animation (Newton’s laws) 
3. A viewing system for curved surfaces with textures (curves and patches)
4. *Two pass or Radiosity method 
5. Ray tracing method for a room / many teapots with different materials 
6. *Volume rendering for a set of tomography slides (NTU hospital etc.) 
7. Image morphing (2D)
8. Sketch system for animation (Teddy system)
9. Oil painting and water color effects for images 
10. 3D morphing and animation with skeleton mapping 
11. Motion retargeting (motion of cats likes that of a human)
12. Hardware Cg acceleration research and applications 
13. Beautifying Images (Color harmonization, face beautification) 
14. Others—Human Computer Interface, Installation Arts, Water Rendering etc.

Reference papers available.  
*:considered difficult

**Project:** Oil painting and water color effects for images  
**Team member:** Carson(Only one)  
**Programming Language:** Java    
**IDE:** Eclipse

In this term project, I chose the ninth topic which named “oil painting and water color effects for images” and find the referable paper [1] as my implementation which can switch the input image into the output with effect. In the past tradition, painters developed many painting styles such as Impressionism, Expressionism, Colorist Wash and Pointillism, but they were often limited by the colors and brushes. Today, the pace of life is increasing with technological advancements. In the computer sciences, the pixels can have a large amount of different colors likes the R, G, B values. In this paper, it provides a method that we can use brush strokes to product oil painting property. Besides, if the real nature of curves by drawing with spline increases, then it will present different effects which likes multiple layers.    

However, I have an idea why don’t use camera for image processing likes animation. I also think the image processing is a little simple and boring. Therefore, I want to develop an oil painting video. Finally, I can make it with effects. It can not only image processing but also video processing. 