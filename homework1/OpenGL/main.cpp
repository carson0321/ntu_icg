#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <windows.h>

#include <GL/glew.h>
#include <GL/glfw3.h>


#include "controls.hpp"
#include "TRIModel.h"
#include "Shader.h"
#include "Transform.h"
#include "Light.h"

enum{GAURAUD,PHONG,FLAT}; // enumerate the shading styles, it means GAURAUD=0
#define SHADER_NUM 3 // determine number of shaders

using namespace glm;
using namespace std;

vector<TRIModel> models; // triangle model list

void loadModel(const char* filename){
	TRIModel model;
	model.loadFromFile(filename);
	models.push_back(model);
}

void SetColor(int a, int b){
	/*a = 7; b = 0; black background and white word
	0=black , 1=blue , 2=green , 3=cyan , 4=red , 5=purple , 6=yellow , 7=white */
	system("color 97");
	unsigned short ForeColor = a + 16 * b;
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hCon, ForeColor);
}

int main( void ){
	SetColor(4, 7);
	string choose;
	cout << "Please input the number: 1 or 2" << endl;
	cout << "1 : The model of single object with three shading," << endl;
	cout << "2 : The model of multiple objects with three shading and animation" << endl;
	do{
		cin >> choose;
		if (choose != "1" && choose != "2"){
			cout << "Wrong! Please reinput." << endl;
		}
	} while (choose != "1" && choose != "2");

	//Initialise GLFW
    GLFWwindow* window;
	if (!glfwInit()){
		cout << "Failed to initial GLFW" << endl;
		exit(EXIT_FAILURE);
	}

	//Open a window and create ites OpenGL context
	window = glfwCreateWindow(1024, 768, "Demo", NULL, NULL);
    if (!window){
		cout << "Failed to open GLFW window" << endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);

	//initialize GLEW
	if (glewInit() != GLEW_OK){
		cout << "Failed to initial GLEW" << endl;
		exit(EXIT_FAILURE);
	}




	// Dark blue background    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	if (choose == "1") glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	else if (choose == "2") glClearColor(0.3f, 0.3f, 0.5f, 0.0f);

	// Enable depth test which we can capture the escape key being pressed below
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Read the models and Create and compile our GLSL program from the shaders
	Shader shader[SHADER_NUM];
	if (choose == "1"){
		loadModel("models/balls.tri");
		loadModel("models/Car_road.tri");
	}
	else if (choose == "2"){
		loadModel("models/Plane.tri"); //Floor
		loadModel("models/Church_s.tri");
		loadModel("models/Csie.tri");
		loadModel("models/Easter.tri"); //Stone
		loadModel("models/Plant.tri");
		loadModel("models/Car_road.tri");
		loadModel("models/Kangaroo.tri");
		loadModel("models/Fighter.tri");
		loadModel("models/balls.tri");
	}

	shader[GAURAUD].CreateProgram("gauraud_shading.vertexshader", "gauraud_shading.fragmentshader");
	shader[PHONG].CreateProgram("phong_shading.vertexshader", "phong_shading.fragmentshader");
	shader[FLAT].CreateProgram("flat_shading.vertexshader", "flat_shading.fragmentshader");

	// get associated handle for uniform variables and attribute variables in the shader program
	string univars[6] = { "PVM", "V", "M", "normat", "ligposwld", "ligcolor" };
	string attrvars[3]={"vertposmdl", "vertnormdl", "vertcolor"};
	for(int s = 0; s < SHADER_NUM; ++s){
		for(int i = 0; i < 6; ++i)
			shader[s].GetUniformLocation(univars[i]);
		for(int i = 0; i < 3; ++i)
			shader[s].GetAttributeLocation(attrvars[i]);
	}

	// Create vertex buffer objects and bind them to attribute handle
	VBO vertbuf(3, GL_FLOAT), colorbuf(3, GL_FLOAT), normalbuf(3, GL_FLOAT);
	int numvert = 0;
	for(size_t i = 0; i < models.size(); ++i)
		numvert += models[i].vertices.size();
	GLsizeiptr reqmem = numvert * sizeof(vec3);
	vertbuf.Alloc(reqmem);
	colorbuf.Alloc(reqmem);
	normalbuf.Alloc(reqmem);
	for(size_t i = 0; i < models.size(); ++i){
		GLsizeiptr memsize = models[i].vertices.size() * sizeof(vec3);
		vertbuf.Append(memsize, &(models[i].vertices[0]));
		colorbuf.Append(memsize, &(models[i].forecolors[0]));
		normalbuf.Append(memsize, &(models[i].normals[0]));
	}
	for(int s = 0; s < SHADER_NUM; ++s){
		shader[s].BindVBO(&vertbuf, "vertposmdl");
		shader[s].BindVBO(&colorbuf,"vertcolor");
		shader[s].BindVBO(&normalbuf, "vertnormdl");
	}

	// Create Lights, and bind them to uniform vector handle
	if (choose == "1"){
		Light lig(vec3(4, -4, 4), vec3(1.0, 1.0, 1.0)),
			lig1(vec3(-4, 4, 4), vec3(1.0, 1.0, 1.0));
		for (int s = 0; s < SHADER_NUM; ++s){
			shader[s].BindVector(&(lig.pos), "ligposwld");
			shader[s].BindVector(&(lig.color), "ligcolor");
		}
	}
	else if (choose == "2"){
		Light lig(vec3(-4, 4, 4), vec3(1.0, 1.0, 1.0));
		for (int s = 0; s < SHADER_NUM; ++s){
			shader[s].BindVector(&(lig.pos), "ligposwld");
			shader[s].BindVector(&(lig.color), "ligcolor");
		}
	}


	// Create Transform, and bind them to uniform matrix handle
	Transform transform;
	for(int s = 0; s < SHADER_NUM; ++s){
		shader[s].BindMatrix(&(transform.pvm), "PVM");
		shader[s].BindMatrix(&(transform.modelmat), "M");
		shader[s].BindMatrix(&(transform.viewmat), "V");
		shader[s].BindMatrix(&(transform.normat), "normat");
	}
	//the viewing matrix (camera setting) is currently fixed
	//you can also change it in the render loop
	/*First, cameraPosition the position of your camera, in world space
	Second, cameraTarget where you want to look at, in world space
    Third, upVector probably glm::vec3(0,1,0), but (0,-1,0) would make you looking upside-down,
	which can be great too.
	Also mean that eye: position, reference point: look, up: camera orientation*/
	transform.SetViewMatrix(vec3(0, 0, 6.0), vec3(0.0, 0.0, -40.0), vec3(0.0, 1.0, 0.0));

	// start to render
	vec3 rot(0.0, 0.0, 0.0);

	CtrlParam ctrlparam;

	//variable on animation
	int transformshading=0;
	float controlleftmove=0,controlrighttmove=0,controlbehindtmove=0,ballcounter=0,tempshading=0;
    //while (!glfwWindowShouldClose(window)){
	do{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// the projection may changing
#if 0   //If 0 meams that this block don't compile
		// update transformation according to keyboard/mouse control, see control.hpp/cpp
		CatchEvent(window);
		UpdateCtrlParams(ctrlparam);
		transform.SetProjectionMatrix(ctrlparam.dfov);
		transform.SetModelMatrix(ctrlparam.size, models[0].center, ctrlparam.trans, ctrlparam.rot);
#endif
/*#if 1 //x=0.8 left,right  y=-0.7 up,down  z=2.79 front,behind
		transform.SetProjectionMatrix(0);
		transform.SetModelMatrix(0.002739, models[0].center, vec3(0.8, -0.7, 2.79), vec3(0, rot.y, 0) );
#endif
		transform.UpdatePVM();
		transform.UpdateNormalMatrix();
		shader[GAURAUD].Draw(GL_TRIANGLES, vertbuf.offset[0] / sizeof(vec3), models[0].vertices.size());*/

		if (choose == "1"){
			for (int i = 0; i < 3; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.002739, models[0].center, vec3(0.8 - i, -0.7, 2.79), vec3(0, rot.y, 0));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				if (i == 0) shader[GAURAUD].Draw(GL_TRIANGLES, vertbuf.offset[0] / sizeof(vec3), models[0].vertices.size());
				else if (i == 1) shader[PHONG].Draw(GL_TRIANGLES, vertbuf.offset[0] / sizeof(vec3), models[0].vertices.size());
				else if (i == 2) shader[FLAT].Draw(GL_TRIANGLES, vertbuf.offset[0] / sizeof(vec3), models[0].vertices.size());
			}

			for (int i = 0; i < 3; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.002739, models[1].center, vec3(1 - i, 0.5, 3.5), vec3(0, rot.y, 0));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				if (i == 0) shader[GAURAUD].Draw(GL_TRIANGLES, vertbuf.offset[1] / sizeof(vec3), models[1].vertices.size());
				else if (i == 1) shader[PHONG].Draw(GL_TRIANGLES, vertbuf.offset[1] / sizeof(vec3), models[1].vertices.size());
				else if (i == 2) shader[FLAT].Draw(GL_TRIANGLES, vertbuf.offset[1] / sizeof(vec3), models[1].vertices.size());
			}
			rot.y += 0.1;
		}
		else if (choose == "2"){
			//Floor effect
			transform.SetProjectionMatrix(0);
			transform.SetModelMatrix(0.1, models[0].center, vec3(0.5, -0.7, 2.79), vec3(270, 0, 0));
			transform.UpdatePVM();
			transform.UpdateNormalMatrix();
			shader[PHONG].Draw(GL_TRIANGLES, vertbuf.offset[0] / sizeof(vec3), models[0].vertices.size());

			//Building effect (Church or CISE)
			for (int i = 0; i < 3; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.13, models[1].center, vec3(-1 * i + 1, -0.35, 2.5), vec3(270, 0, (i-1)*15+120));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				shader[i].Draw(GL_TRIANGLES, vertbuf.offset[1] / sizeof(vec3), models[1].vertices.size());
			}
			/*transform.SetProjectionMatrix(0);
			transform.SetModelMatrix(0.002739, models[2].center, vec3(-0.4, -0.4, 2.79), vec3(-90, 0, 30));
			transform.UpdatePVM();
			transform.UpdateNormalMatrix();
			shader[PHONG].Draw(GL_TRIANGLES, vertbuf.offset[2] / sizeof(vec3), models[2].vertices.size());*/

			//Stone effect
			for (int i = 0; i < 3; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.3, models[3].center, vec3(-2*i+2, 0, 0), vec3(270, 0, (i-1)*15));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				shader[i].Draw(GL_TRIANGLES, vertbuf.offset[3] / sizeof(vec3), models[3].vertices.size());
			}

			//Plant effect
			for (int i = 0; i < 17; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.005, models[4].center, vec3(2.4-0.3*i, -0.6, 1.5), vec3(270, 0, 0));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				shader[transformshading].Draw(GL_TRIANGLES, vertbuf.offset[4] / sizeof(vec3), models[4].vertices.size());
			}

			//Car and Kangaroo effect
			transform.SetProjectionMatrix(0);
			transform.SetModelMatrix(0.00135, models[5].center, vec3(2 + controlleftmove, -0.55, 3.5), vec3(270, 0, 0));
			transform.UpdatePVM();
			transform.UpdateNormalMatrix();
			shader[transformshading].Draw(GL_TRIANGLES, vertbuf.offset[5] / sizeof(vec3), models[5].vertices.size());
			transform.SetProjectionMatrix(0);
			transform.SetModelMatrix(0.0000005, models[6].center, vec3(2.07 + controlleftmove, -0.48, 3.5), vec3(270, 0, -90));
			transform.UpdatePVM();
			transform.UpdateNormalMatrix();
			shader[transformshading].Draw(GL_TRIANGLES, vertbuf.offset[6] / sizeof(vec3), models[6].vertices.size());

			//Fighter effect
			transform.SetProjectionMatrix(0);
			transform.SetModelMatrix(0.02, models[7].center, vec3(-1.5 + controlrighttmove, 1, 2.79), vec3(rot.x, 0, 0));
			transform.UpdatePVM();
			transform.UpdateNormalMatrix();
			shader[transformshading].Draw(GL_TRIANGLES, vertbuf.offset[7] / sizeof(vec3), models[7].vertices.size());

			for (int i = 0; i<10; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.01, models[7].center, vec3(-3 + i, 2, 4 - controlbehindtmove), vec3(-90, 0, 90));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				shader[PHONG].Draw(GL_TRIANGLES, vertbuf.offset[7] / sizeof(vec3), models[7].vertices.size());
			}

			//Ball effect
			for (int i = 0; i<4; i++){
				transform.SetProjectionMatrix(0);
				transform.SetModelMatrix(0.001, models[8].center, vec3(0.8 - ballcounter, -0.7, 4.3), vec3(0, rot.y, 0));
				transform.UpdatePVM();
				transform.UpdateNormalMatrix();
				shader[transformshading].Draw(GL_TRIANGLES, vertbuf.offset[8] / sizeof(vec3), models[8].vertices.size());
				ballcounter += 0.5;
				if (i == 3) ballcounter = 0;
			}

			//transform shading
			if (tempshading < 3){
				transformshading = tempshading;
				tempshading += 0.01;
			}
			else{
				transformshading = 0;
				tempshading = 0;
			}

			//move on animation
			if (controlleftmove < -4) controlleftmove = 0;
			//else controlleftmove = controlleftmove - 0.004;
			else controlleftmove = controlleftmove - 0.008;
			if (controlrighttmove > 4.5) controlrighttmove = 0;
			//else controlrighttmove = controlrighttmove + 0.005;
			else controlrighttmove = controlrighttmove + 0.01;

			if (controlbehindtmove > 13) controlbehindtmove = 0;
			else controlbehindtmove = controlbehindtmove + 0.0085;
			rot.x += 0.3;
			rot.y += 0.3;
			rot.z += 0.15;
		}

		//Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }// Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		glfwWindowShouldClose(window) == 0);

    glfwDestroyWindow(window);
    glfwTerminate();

	return 0;
}
